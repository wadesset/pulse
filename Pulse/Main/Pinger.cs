﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Server.Kestrel.Core.Internal.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Pulse.Context;
using Pulse.Models;

namespace Pulse.Main
{
    public static class Pinger
    {
        public static ApplicationContext Context { get; set; }
        public static Thread Thread { get; private set; }
        private static bool Send = false;

        private static async Task Ping(Site site)
        {
            
                Ping pingSender = new Ping();
                PingOptions options = new PingOptions {DontFragment = true};
                PingReply reply = null;
                // Use the default Ttl value which is 128,
                // but change the fragmentation behavior.

                // Create a buffer of 32 bytes of data to be transmitted.
                string data = "a";
                byte[] buffer = Encoding.ASCII.GetBytes(data);
                int timeout = 10;
                try
                {
                    reply = pingSender.Send(site.Sitename, timeout, buffer, options);
                }
                catch (PingException ex)
                {
                    Console.WriteLine(site.Sitename);
                    Console.WriteLine(ex.Message);
                }

                if (reply != null && reply.Status == IPStatus.Success)
                {
                    Console.WriteLine("Address: {0}", reply.Address.ToString());
                    Console.WriteLine("RoundTrip time: {0}", reply.RoundtripTime);
                    Console.WriteLine("Time to live: {0}", reply.Options.Ttl);
                    Console.WriteLine("Don't fragment: {0}", reply.Options.DontFragment);
                    Console.WriteLine("Buffer size: {0}", reply.Buffer.Length);
                }

                if (reply != null)
                {
                    site.Active = true;
                    try
                    {
                        Context.Update(site);
                    }
                    catch (ObjectDisposedException ex)
                    {
                        Console.WriteLine(ex.Message);
                    }

                    await Context.SaveChangesAsync();
                }
        }

        //Принудительный запуск нового цикла проверки доступности списка сайтов
        public static async Task Start()
        {
            //if (Send != false)
            //{
            //    Thread.Sleep(Context.Settings.Last().Time * 1000 * 60);
            //}
            //Thread = new Thread(BeginPing);
            //Thread.Start();
            await BeginPing();
            //Send = true;
        }

        private static async Task BeginPing()
        {
                List<Site> siteList = Context.Sites.ToListAsync().Result;
                foreach (Site site in siteList)
                {
                    if (site != null)
                        await Ping(site);
                }
        }
    }
}