﻿using Microsoft.EntityFrameworkCore;
using Pulse.Models;

namespace Pulse.Context
{
    public class ApplicationContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Site> Sites { get; set; }
        public DbSet<Setting> Settings { get; set; }

        public ApplicationContext(DbContextOptions options) : base(options)
        {
        }
    }
}
