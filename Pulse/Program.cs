﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace Pulse
{
    public class Program
    {
        private static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}