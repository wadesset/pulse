﻿namespace Pulse.Models
{
    public class Site
    {
        public int Id { get; set; }
        public string Sitename { get; set; }
        public bool Active { get; set; }
    }
}
