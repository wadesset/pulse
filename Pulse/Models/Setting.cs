﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Pulse.Models
{
    public class Setting
    {
        public int Id { get; set; }

        [Display(Name = "Time (the time at which the list of sites will be repeated) in minuts")]
        public int Time { get; set; }
    }
}
