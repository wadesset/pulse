﻿using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Pulse.Context;
using Pulse.Main;
using Pulse.Models;

namespace Pulse.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationContext db;

        public HomeController(ApplicationContext context)
        {
            db = context;
            Pinger.Context = db;
        }

        [Authorize]
        public async Task<IActionResult> Index()
        {
            ViewBag.Sites = await db.Sites.ToListAsync();
            if (db.Settings.ToListAsync().Result.Count > 0)
                return View("Index", db.Settings.Last());
            return View("Index", new Setting());
        }

        public async Task<IActionResult> Home()
        {
            //if (Pinger.Thread == null)
            await Pinger.Start();
            ViewBag.Sites = await db.Sites.ToListAsync();
            return View("Home");
        }

        public async Task<IActionResult> AddSite(string site)
        {
            await db.Sites.AddAsync(new Site() {Id = db.Sites.ToListAsync().Result.Count + 1, Sitename = site});
            await db.SaveChangesAsync();
            ViewBag.Sites = await db.Sites.ToListAsync();
            return View("Index", db.Settings.ToListAsync().Result.Last());
        }

        public async Task<IActionResult> ClearSite()
        {
            db.Sites.RemoveRange(db.Sites);
            await db.SaveChangesAsync();
            ViewBag.Sites = await db.Sites.ToListAsync();
            return View("Index");
        }

        public async Task<IActionResult> SaveSetting(Setting setting)
        {
            if (db.Settings.ToListAsync().Result.Count > 0)
            {
                //setting.Id = 1;//ДОДЕЛАТЬ!!! Добавляет новую настройку. Сделать хотя бы на Last
                await db.Settings.AddAsync(setting);
            }
            else 
            {
                setting.Id = db.Settings.ToListAsync().Result.Count + 1;
                await db.Settings.AddAsync(setting);
            }
            await db.SaveChangesAsync();
            ViewBag.Sites = await db.Sites.ToListAsync();
            return View("Index", setting);
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public async Task<IActionResult> Start()
        {
            await Pinger.Start();

            ViewBag.Sites = await db.Sites.ToListAsync();
            return View("Home");
        }
    }
}
